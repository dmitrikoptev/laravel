@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul class="list-group list-group-flush">
                        @foreach ($articles as $article)
                    
                            <li class="list-group-item">
                                <div class="row justify-content-end">

                                    <div class= col-8>
                                        <a href="{{ route('article.show', $article->id)}}">{{ $article->title }}</a>
                                    </div>

                                    <div class = col-2>
                                        <a class="btn btn-primary" href="{{route('article.edit',$article->id)}}">Edit</a>
                                    </div>

                                    <div class = col-2>
                                        <form action="{{route('article.destroy', $article->id)}}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger" onclick="return confirm('are you sure')">Delete</a>
                                        </form>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <a class="btn btn-success" href="{{route('article.create')}}">Add new article</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
