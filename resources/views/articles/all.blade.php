@extends('layouts.app')

@section('content')
    <h1> Articles({{$total}}) </h1>
    @foreach ($articles as $article)
<h2><a href="{{route('article.show', $article->id)}}">{{$article->title}}</a></h2>
<p>{{$article->description}}</p>
    @endforeach
</div>
@endsection