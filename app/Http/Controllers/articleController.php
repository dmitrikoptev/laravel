<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Auth;
//use Illuminate\Routing\Router;
use App\Article;


class articleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get articles from database
        $articles = Article::all();
        //pass articles to "articles all" view
        return view('articles.all',$articles); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //create new instance of article from data
        $article = new Article();
        //save data
        $this->validate($request,[
            'title'=>'required|string|max:255|min:1',
            'description'=>'required|string|max:255|min:1',
            'content'=>'required|string|max:10000|min:1'
        ]);

        $article->$new Article();

        $article->title = $request->title;
        $article->description = $request->description;
        $article->content = $request->content;
        $article->author_id = Auth::id();

        //store insatance data as new rows in database
        $article->save();
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $article=Article::find($id);
        //$route =Route::current();
        return view('articles.single')->with('article',$article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //TODO 

        //get the article 
        $article=Article::find($id);
        //show form for editing and fill it with data from database
        return view('articles.edit')->with('article',$article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $article=Article::find($id);
        $article->title = $request->title;    
        $article->description = $request->description; 
        $article->content = $request->content;  
        $article->save();
        return redirect()->route('article.show',$id);   

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $article = Article::find($id);
        //check if the article belongs to the cuurent logged in user
        if($article->author_id == Auth::id()){
            $article->delete();
            return redirect()->route('home');
        }else{
            return view('404')->with(['status'=>'401','error'=>'Bad credentials']);
        }
    }
}
